package pl.listaserwerow;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GeneralCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		//Jeśli nie podano argumentów komendy,
		//plugin wyswietli na ekranie link do dokumentacji API i pomoc
		if(args.length < 1) {
			showHelp(sender);
			return true;
		}
		
		//switch organizujący różne warianty tej komendy
		switch(args[0]) {
		//odwołanie do http://www.listaserwerow.pl/api/server/:srv_id
		case "server":
			if(args.length!=2) {
				sender.sendMessage("Uzycie: /ls server <srv_id>");
				return false;
			}
			serverCommand(sender, args[1]);
			break;
			
		default: 
			sender.sendMessage("Nieznana komenda.");
			return false;
		}
		
		return true;
	}
	
	private void serverCommand(final CommandSender player, final String arg) {
		/*
		 * ten niewielki blok try/catch powstrzymuje plugin przed połączeniem się
		 * z listąserwerów jeśli podany srv_id jest niezgodny z założeniami. Nasz serwer
		 * i tak by odrzucił takie zapytanie, dla nas obu lepiej, żeby to tu było! :)
		*/
		try {
			int srv_id = Integer.parseInt(arg);
			if(srv_id<1) throw new NumberFormatException();
		} catch (NumberFormatException numex) {
			player.sendMessage("Argument 2 musi byc liczba calkowita(INT).");
			return;
		}
		
		//tworzę Asynchroniczne zadanie które łączy się z serwerem...
		new BukkitRunnable() {
			
			@Override
			public void run() {
				//tak wygląda adres który nasz plugin zaraz odwiedzi:
				final String api = "http://www.listaserwerow.pl/api/server/%s";
				
				//surowa odpowiedz od serwera pojawi się pod postacią zmiennej raw_response
				String raw_response = SimpleConnection.executePost(String.format(api, arg), "");
				/* Użyta funkcja pl.listaserwerow.SimpleConnection.executePost prosi o 2 argumenty:
				 * link i parametry związane z zapytaniem POST. Obydwa typu String. 
				 * Pierwszy widać czarno na białym jak wygląda, drugi nas nie interesuje (akurat w tym przypadku),
				 * więc zostawiam go jako pusty łańcuch.
				 */
				
				if(!(player instanceof Player)) {
					//jeśli komende wykona ktoś z poziomu konsoli, dajmy sobie spokój,
					//wyświetlamy surowy ciąg znaków.
					player.sendMessage(raw_response);					
				} else {
//					//Jednak wiadomo - spodziewamy się odpowiedzi w formacie JSON.
//					//Więc sformatujmy sobie to ładnie...
					JsonOnChat.display(raw_response, player);
				}
			}
		}.runTaskAsynchronously(Main.getInstance());
		/* Warto nadmienić że powyższy blok został wykonany Asynchronicznie.
		 * To znaczy mniej więcej tyle, że inne zadania w tym czasie były wykonywane,
		 * ma to swoje plusy i minusy, dużym plusem jest to że gracze nie muszą czekać na odpowiedz
		 * od listy serwerów. Gdyby funkcja została wykonana synchronicznie to do czasu jej ukończenia
		 * nie możnaby robić na serwerze NIC. To bardzo niebezpieczne...
		 * Minusy są typowe dla tego rozwiązania, ale niewielkie. Po prostu trzeba to dobrze
		 * wykorzystać i nie będzie żadnego problemu. */
		
		//to tyle!
	}
	
	/**
	 * @param player Gracz któremu pojawi się wiadomość z pomocą
	 */
	private void showHelp(CommandSender player) {
		player.sendMessage("= = = = POMOC = = = =");
		player.sendMessage("/ls server <srv_id>");
		player.sendMessage("/ls user <usr_id>");
		player.sendMessage(" ");
		player.sendMessage("= = = = DOKUMENTACJA = = = =");
		player.sendMessage("https://docs.google.com/document/d/1esLMgM7Hng31jrzS5un9Z-PgeXWy0JlXhYqv3nfvsoY/");
	}

}
