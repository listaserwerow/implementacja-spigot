package pl.listaserwerow;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SimpleConnection {

	/**
	 * @author duffymo
	 * @param address Adres URL z którym połączy się plugin
	 * @param params Parametry POST
	 * @return Zwraca łańcuch znaków które odczyta z podanego adresu
	 * Funkcja nie mojego autorstwa.
	 * To jedynie przykład, zródło tu: https://stackoverflow.com/a/1359700
	 */
	public static String executePost(String address, String params) {
		HttpURLConnection connection = null;
		
		try {
			//Create connection
			URL url = new URL(address);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
			"application/x-www-form-urlencoded");
			
			connection.setRequestProperty("Content-Length", 
			Integer.toString(params.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");  
			
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			
			//Send request
			DataOutputStream wr = new DataOutputStream (
			connection.getOutputStream());
			wr.writeBytes(params);
			wr.close();
			
			//Get Response  
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
