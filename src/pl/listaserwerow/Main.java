package pl.listaserwerow;

import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Main instance;
	
	@Override
	public void onEnable() {
		instance = this;
		
		getLogger().log(Level.INFO, "Przykladowa Implementacja Listaserwerow.pl");
		
		instance.getCommand("ls").setExecutor(new GeneralCommand());
	}
	
	public static JavaPlugin getInstance() {
		return instance;
	}
	
}
