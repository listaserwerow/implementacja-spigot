package pl.listaserwerow;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.command.CommandSender;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonOnChat {
	
	@SuppressWarnings("unchecked")
	public static void display(String jsonText, CommandSender player) {

		JSONObject json = null;
		try {
			json = (JSONObject)new JSONParser().parse(jsonText);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//Tak możecie pobrać wartość z JSON'a
		Boolean success = (Boolean) json.get("success");
		String detail = (String) json.get("detail");
		//Fajnie jest je castować to możecie ich używać w swoim kodzie,
		//do różnych szalonych rzeczy ^^

		player.sendMessage("> > > ZAPYTANIE");
		player.sendMessage("success: "+success+"; detail: "+detail);
		
		JSONArray data = (JSONArray) json.get("data");
		if(success == true && data != null) {
			//szczegoly dotyczace zapytania znajduja sie w {[...], data:[{ tutaj }]}
			JSONObject child = (JSONObject) data.get(0);
			
			Map<String, Object> values = new HashMap<String, Object>();
			for(String key : (Set<String>)child.keySet()) {
				values.put(key, child.get(key));
				//TODO: zabezpiecz przed niespodziewaną nieskończoną pętlą
			}
			
			player.sendMessage("> > > WLASCIWOSCI OBJEKTU DATA");
			for(Entry<String, ?> entry : values.entrySet()) {
				player.sendMessage(entry.getKey()+": "+entry.getValue());
			}
		}
	}
	
}
